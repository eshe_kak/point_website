const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: "./src/index.tsx",
  output: {
    filename: "[name].bundle.js",
    chunkFilename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  devtool: "source-map",
  resolve: {
    extensions: [".js", ".json", ".ts", ".tsx"]
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        loader: "awesome-typescript-loader"
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      // {
      //     enforce: 'pre',
      //     test: /\.css$/,
      //     exclude: /node_modules/,
      //     loader: 'typed-css-modules'
      // },
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            // loader: 'css-loader',
            // options: {
            //     module: true,
            //     importLoaders: 1,
            //     localIdentName: '[name]_[local]_[hash:base64]',
            //     sourceMap: true,
            //     minimize: true
            // }
            loader: "typings-for-css-modules-loader",
            options: {
              modules: true,
              namedExport: true,
              localIdentName: "[name]_[local]_[hash:base64]"
              // camelCase: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          process.env.NODE_ENV !== "production"
            ? {
                loader: "style-loader"
              }
            : MiniCssExtractPlugin.loader,
          {
            // loader: 'css-loader',
            // options: {
            //     module: true,
            //     importLoaders: 1,
            //     localIdentName: '[name]_[local]_[hash:base64]',
            //     sourceMap: true,
            //     minimize: true
            // }
            loader: "typings-for-css-modules-loader",
            options: {
              modules: true,
              namedExport: true,
              localIdentName: "[name]_[local]_[hash:base64]"
              // camelCase: true
            }
          },
          {
            loader: "sass-loader"
          }
        ]
      }
    ]
  },
  devServer: {
    historyApiFallback: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};
