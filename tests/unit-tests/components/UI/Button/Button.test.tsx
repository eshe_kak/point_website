// import * as React from 'react';
// import { shallow, configure } from 'enzyme';
// import * as Adapter from 'enzyme-adapter-react-16';

// import Button from '../../../../src/components/UI/Button/Button';

// configure({ adapter: new Adapter() });

// // Button`s Test On Starting Page #1

// const buttonSignUpIsFilledStartingPage = shallow(<Button text={'Sign Up'} isFilled={true} nameOfPage={'StartingPage'} />);

// const buttonLogInNotFilledStartingPage = shallow(<Button text={'Log In'} isFilled={false} nameOfPage={'StartingPage'} />);

// const buttonNotFilledDefault = shallow(<Button text={'Default'} isFilled={false} nameOfPage={'Default'} />);

// describe('Button`s Test On Starting Page #1: Check all types of Button`s classNames', () => {

//     // Button`s classNames

//     test('Contains "Sign Up" text', () => {
//         expect(buttonSignUpIsFilledStartingPage.contains('Sign Up')).toBe(true);
//     });

//     test('Contains classes such as "Button Button_isFilled_true Button_size_startingPage"', () => {
//         expect(buttonSignUpIsFilledStartingPage.hasClass('Button Button_isFilled_true Button_size_startingPage')).toBe(true);
//     });

//     test('Contains "Log In" text', () => {
//         expect(buttonLogInNotFilledStartingPage.contains('Log In')).toBe(true);
//     });

//     test('Contains classes such as "Button Button_isFilled_false Button_size_startingPage"', () => {
//         expect(buttonLogInNotFilledStartingPage.hasClass('Button Button_isFilled_false Button_size_startingPage')).toBe(true);
//     });

//     const defaultButton: any = <div className='Button Button_isFilled_false'>
//         <span className='Button-Text Button-Text_color_mainColor'>Default</span>
//     </div>;

//     test('This button is default', () => {
//         expect(buttonNotFilledDefault.contains(defaultButton)).toBe(true);
//     });

// });
