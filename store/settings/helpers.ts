import moment from "moment";

export enum MyGender {
  FEMALE = 0,
  MALE = 1
}

export enum YourAge {
  UNDEFINED = -1, // not selected
  YOUNG = 1, // 18 - 22
  STILL_YOUNG = 2, // 23 - 27
  MIDDLE = 3, // 28 - 35
  PREMATURE = 4, // 36 - 45
  MATURE = 5, // 46+
  ALL = 6 // ALL AGES
}

export enum YourGender {
  ALL = -1, // doesn`t matter
  FEMALE = 0,
  MALE = 1
}

export interface ISettingsState {
  nickname: string;
  my_age: moment.Moment;
  my_gender: MyGender;
  your_age: YourAge[];
  your_gender: YourGender;
}
