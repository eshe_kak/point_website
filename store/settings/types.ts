export enum SettingsTypes {
  SET_NICKNAME = "[settings] SET_NICKNAME",
  SET_MYAGE = "[settings] SET_MYAGE",
  SET_MYGENDER = "[settings] SET_MYGENDER",
  SET_YOURAGE = "[settings] SET_YOURAGE",
  SET_YOUGENDER = "[settings] SET_YOURGENDER",
  SET_YOURSTODEFAULT = "[settings] SET_YOURSTODEFAULT"
}
