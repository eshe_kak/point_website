import moment from "moment";

import { createAction } from "../helpers";
import { YourAge, YourGender } from "./helpers";
import { SettingsTypes } from "./types";
import { MyGender } from "./helpers";

export const setNickname = (nickname: string) =>
  createAction<typeof SettingsTypes.SET_NICKNAME, typeof nickname>(
    SettingsTypes.SET_NICKNAME,
    nickname
  );

export const setMyAge = (my_age: moment.Moment) =>
  createAction<typeof SettingsTypes.SET_MYAGE, typeof my_age>(
    SettingsTypes.SET_MYAGE,
    my_age
  );

export const setMyGender = (my_gender: MyGender) =>
  createAction<typeof SettingsTypes.SET_MYGENDER, typeof my_gender>(
    SettingsTypes.SET_MYGENDER,
    my_gender
  );

export const setYourAge = (your_age: YourAge[]) =>
  createAction<typeof SettingsTypes.SET_YOURAGE, typeof your_age>(
    SettingsTypes.SET_YOURAGE,
    your_age
  );

export const setYourGender = (your_gender: YourGender) =>
  createAction<typeof SettingsTypes.SET_YOUGENDER, typeof your_gender>(
    SettingsTypes.SET_YOUGENDER,
    your_gender
  );

export const setYoursToDefault = () =>
  createAction<typeof SettingsTypes.SET_YOURSTODEFAULT>(
    SettingsTypes.SET_YOURSTODEFAULT
  );

export type TSetNickname = ReturnType<typeof setNickname>;
export type TSetMyAge = ReturnType<typeof setMyAge>;
export type TSetMyGender = ReturnType<typeof setMyGender>;
export type TSetYourAge = ReturnType<typeof setYourAge>;
export type TSetYourGender = ReturnType<typeof setYourGender>;
export type TSetYoursToDefault = ReturnType<typeof setYoursToDefault>;
