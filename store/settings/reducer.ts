import { Reducer } from "redux";
import moment from "moment";
import { ISettingsState, YourGender, YourAge } from "./helpers";
import { SettingsTypes } from "./types";
import {
  TSetNickname,
  TSetMyAge,
  TSetMyGender,
  TSetYourAge,
  TSetYourGender,
  TSetYoursToDefault
} from "./actions";

export const initialState: ISettingsState = {
  nickname: "",
  my_age: moment(),
  my_gender: null,
  your_age: [],
  your_gender: null
};

type ActionType =
  | TSetNickname
  | TSetMyAge
  | TSetMyGender
  | TSetYourAge
  | TSetYourGender
  | TSetYoursToDefault;

const reducer: Reducer<ISettingsState> = (
  state = initialState,
  action: ActionType
) => {
  switch (action.type) {
    case SettingsTypes.SET_NICKNAME:
      return {
        ...state,
        nickname: action.payload
      };
    case SettingsTypes.SET_MYAGE:
      return {
        ...state,
        my_age: action.payload
      };
    case SettingsTypes.SET_MYGENDER:
      return {
        ...state,
        my_gender: action.payload
      };
    case SettingsTypes.SET_YOURAGE:
      return {
        ...state,
        your_age: action.payload
      };
    case SettingsTypes.SET_YOUGENDER:
      return {
        ...state,
        your_gender: action.payload
      };
    case SettingsTypes.SET_YOURSTODEFAULT:
      return {
        ...state,
        your_age: [],
        your_gender: null
      };
    default:
      return state;
  }
};

export { reducer as settingsReducer };
