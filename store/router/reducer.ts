import { Reducer } from "redux";
import { IRoutersState } from "./helpers";
import { RoutersTypes } from "./types";
import { TSetPrevURL } from "./actions";

export const initialState: IRoutersState = {
  previousURL: "/"
};

const reducer: Reducer<IRoutersState> = (
  state = initialState,
  action: TSetPrevURL
) => {
  switch (action.type) {
    case RoutersTypes.SET_PREVURL:
      return {
        ...state,
        previousURL: action.payload
      };
    default:
      return state;
  }
};

export { reducer as routerReducer };
