import { createAction } from "../helpers";
import { RoutersTypes } from "./types";

export const setPrevURL = (prevURL: string) =>
  createAction<typeof RoutersTypes.SET_PREVURL, typeof prevURL>(
    RoutersTypes.SET_PREVURL,
    prevURL
  );

export type TSetPrevURL = ReturnType<typeof setPrevURL>;
