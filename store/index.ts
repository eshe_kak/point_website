import { combineReducers, Dispatch, Action, AnyAction } from "redux";

import { ISettingsState } from "./settings/helpers";
import { settingsReducer } from "./settings/reducer";

import { IRoutersState } from "./router/helpers";
import { routerReducer } from "./router/reducer";

export interface IApplicationState {
  settingsReducer: ISettingsState;
  routerReducer: IRoutersState;
}

export interface ConnectedReduxProps<A extends Action = AnyAction> {
  dispatch: Dispatch<A>;
}

export const rootReducer = combineReducers<IApplicationState>({
  settingsReducer,
  routerReducer
});
