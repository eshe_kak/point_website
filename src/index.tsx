// tslint:disable-next-line:max-line-length

import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import { applyMiddleware, combineReducers, createStore } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";

import {
  ConnectedRouter,
  push,
  routerMiddleware,
  routerReducer
} from "react-router-redux";

import { rootReducer } from "../store/index";

import App from "./App";

// // Create a history of your choosing (we're using a browser history in this case)
// const history = createHistory();

// // Build the middleware for intercepting and dispatching navigation actions
// const middleware = routerMiddleware(history);

const store = createStore(rootReducer, devToolsEnhancer({}));

// const store = createStore(
//     combineReducers({
//         rootReducer,
//         router: routerReducer
//     }),
//     applyMiddleware(middleware)
//   );

render(
  // <React.StrictMode>
  <Provider store={store}>
    <BrowserRouter>
      {/* <ConnectedRouter history={history}> */}
      <App />
      {/* </ConnectedRouter> */}
    </BrowserRouter>
  </Provider>,
  document.getElementById("app")
);
