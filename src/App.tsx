import "normalize.css";
import * as React from "react";
import { Route, Switch } from "react-router-dom";

import styles from "./App.scss";

import asyncComponent from "../logic/hoc/asyncComponent/asyncComponent";
import FullSignUp from "./components/Routes/FullSignUp/FullSignUp";

import {
  FULL_SIGNUP,
  HOME,
  LOGIN,
  STARTING_PAGE
} from "../logic/constants/_routeUrls";

interface State {
  currentPathname: string;
}

const AsyncStartingPage: any = asyncComponent(() => {
  return import("./containers/StartingPage/StartingPage");
});

const AsyncErrorPage: any = asyncComponent(() => {
  return import("./containers/ErrorPage/ErrorPage");
});

const AsyncSignUp_3: any = asyncComponent(() => {
  return import("./containers/SignUp_3/SignUp_3");
});

const AsyncHomeScreen: any = asyncComponent(() => {
  return import("./containers/HomeScreen/HomeScreen");
});

class App extends React.Component<{}, State> {
  constructor(props: null) {
    super(props);

    this.state = {
      currentPathname: document.location.pathname
    };
  }

  // private allowRouting(pathname: string):boolean {

  //     this.setState(prevState => {

  //         if (prevState.currentPathname === pathname) {
  //             // isAllowed = true;
  //             return {
  //                 currentPathname: pathname
  //             }
  //         }
  //     });

  //     return pathname === document.location.pathname;
  // }

  public render() {
    return (
      <div className={styles.App}>
        <Switch>
          <Route
            exact={true}
            path={STARTING_PAGE}
            component={AsyncStartingPage}
          />
          <Route path={FULL_SIGNUP} component={FullSignUp} />
          <Route
            exact={true}
            path={LOGIN}
            render={() => <AsyncSignUp_3 isSignUp={false} />}
          />
          <Route exacr={true} path={HOME} component={AsyncHomeScreen} />
          <Route component={AsyncErrorPage} />
        </Switch>
      </div>
    );
  }
}

export default App;
