import "normalize.css";
import React, { Component } from "react";

import styles from "./HomeScreen.scss";

import Header from "../../components/UI/Header/Header";
import PointButton from "../../components/UI/PointButton/PointButton";
import SideBarMenu from "../../components/UI/SideBarMenu/SideBarMenu";
import Suggestion from "../../components/UI/Suggestion/Suggestion";

interface State {
  isActive: boolean;
}

class HomeScreen extends Component<any, State> {
  private hintRef: React.RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);

    this.state = {
      isActive: false
    };

    this.hintRef = React.createRef();
  }

  // Apply this style after disappear animation
  private makeHitToBeDisappeared() {
    const className = [
      styles.WorkingSpace_Hint__layout,
      styles.WorkingSpace_Hint__layout_inner,
      styles.WorkingSpace_Hint__size,
      styles.WorkingSpace_Hint__style,
      styles.WorkingSpace_Hint__style_animation,
      styles.WorkingSpace_Hint__style_opacity0
    ];

    this.hintRef.current.className = className.join(" ");
  }

  // Apply this style before disappear animation
  private applyAnimForHint() {
    const className = [
      styles.WorkingSpace_Hint__layout,
      styles.WorkingSpace_Hint__layout_inner,
      styles.WorkingSpace_Hint__size,
      styles.WorkingSpace_Hint__style,
      styles.WorkingSpace_Hint__style_animation
    ];

    this.hintRef.current.className = className.join(" ");
  }

  public render() {
    const hint = (
      <span
        className={[
          styles.WorkingSpace_Hint__layout,
          styles.WorkingSpace_Hint__layout_inner,
          styles.WorkingSpace_Hint__size,
          styles.WorkingSpace_Hint__style
        ].join(" ")}
        ref={this.hintRef}
      >
        Tap on point <br /> to share your location
      </span>
    );

    return (
      <div
        className={[
          styles.HomeScreen,
          styles.HomeScreen__style_background
        ].join(" ")}
      >
        <Header />
        <SideBarMenu />

        <div
          className={[
            styles.HomeScreen_WorkingSpace__size,
            styles.HomeScreen_WorkingSpace__style,
            styles.HomeScreen_WorkingSpace__style_text,
            styles.HomeScreen_WorkingSpace__layout,
            styles.HomeScreen_WorkingSpace__layout_inner
          ].join(" ")}
        >
          {hint}

          <PointButton
            onActive={() => {
              this.applyAnimForHint();
              setTimeout(() => this.makeHitToBeDisappeared(), 2000);
            }} />

          {/* <Suggestion /> */}
        </div>
      </div>
    );
  }
}

export default HomeScreen;
