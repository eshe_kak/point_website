import "normalize.css";
import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";

import { IApplicationState } from "../../../store/index";
import {
  ISettingsState,
  YourAge,
  YourGender
} from "../../../store/settings/helpers";

import { setPrevURL } from "../../../store/router/actions";
import { IRoutersState } from "../../../store/router/helpers";

import Button from "../../components/UI/Button/Button";
import LogInSuggestion from "../../components/UI/LogInSuggestion/LogInSuggestion";
import AgeSelector from "../../components/UI/Preferences/AgeSelector/AgeSelector";
import GenderSelector from "../../components/UI/Preferences/GenderSelector/GenderSelector";

import {
  SIGNUP_1,
  SIGNUP_2,
  SIGNUP_3
} from "../../../logic/constants/_routeUrls";

import {
  ButtonColor,
  ButtonTextColor,
  ButtonType
} from "../../../logic/enums/enums";

import {
  setYourAge,
  setYourGender,
  setYoursToDefault
} from "../../../store/settings/actions";
import styles from "./SignUp_2.scss";

type PropsFromReducers = ISettingsState & IRoutersState;

interface PropsFromState extends PropsFromReducers {
  your_age: YourAge[];
  your_gender: YourGender;

  prevURL: string;
}

interface PropsFromDispatch {
  setYourAge: typeof setYourAge;
  setYourGender: typeof setYourGender;
  setYoursToDefault: typeof setYoursToDefault;

  setPrevURL: typeof setPrevURL;
}

type SignUp_2Props = PropsFromState &
  PropsFromDispatch &
  RouteComponentProps<any>;

interface OtherProps {
  children: (props: SignUp_2Props) => React.ReactNode;
}

interface State {
  isFilled: boolean;
}

class SignUp_2 extends React.Component<SignUp_2Props & OtherProps, State> {
  constructor(props: SignUp_2Props & OtherProps) {
    super(props);

    this.state = {
      isFilled: true
    };
  }

  private onFilled(nextPageSwitch?: Function) {
    const isFilled =
      this.props.your_age.length > 0 &&
      (this.props.your_gender === -1 ||
        this.props.your_gender === 0 ||
        this.props.your_gender === 1);

    this.setState(
      {
        isFilled
      },
      () => (this.state.isFilled ? nextPageSwitch() : null)
    );
  }

  public componentWillUnmount() {
    this.props.setPrevURL(SIGNUP_2);
  }

  public render() {
    return (
      <div className={styles.SignUp}>
        <div
          className={[styles.SignUp_Panel, styles.SignUp_Panel__style].join(
            " "
          )}
        >
          <img
            className={[
              styles.Panel_GoBackIcon__size,
              styles.Panel_GoBackIcon__layout,
              styles.Panel_GoBackIcon__style
            ].join(" ")}
            src="/static/img/goBackIcon.svg"
            onClick={() => this.props.history.push(SIGNUP_1)}
          />

          <span
            className={[
              styles.Panel_HeadText__style,
              styles.Panel_HeadText__layout
            ].join(" ")}
          >
            Sign Up
          </span>

          <span
            className={[
              styles.Panel_SkipText__style,
              styles.Panel_SkipText__layout
            ].join(" ")}
            onClick={() => {
              this.props.setYoursToDefault();
              this.props.history.push(SIGNUP_3);
            }}
          >
            Skip
          </span>

          <img
            className={[
              styles.Panel_ProgressPoint__size,
              styles.Panel_ProgressPointFirst__layout
            ].join(" ")}
            src="/static/img/circleActive.svg"
          />
          <img
            className={[
              styles.Panel_ProgressPoint__size,
              styles.Panel_ProgressPointSecond__layout
            ].join(" ")}
            src="/static/img/circleActive.svg"
          />
          <img
            className={[
              styles.Panel_ProgressPoint__size,
              styles.Panel_ProgressPointThird__layout
            ].join(" ")}
            src="/static/img/circleInactive.svg"
          />
          <span
            className={[
              styles.Panel_InfoText__style,
              styles.Panel_InfoText__layout
            ].join(" ")}
          >
            Who are we looking for?
          </span>

          <span
            className={[
              styles.Panel_WarningText__style,
              styles.Panel_WarningText__gender_layout
            ].join(" ")}
          >
            Gender
          </span>

          <GenderSelector
            selectedGender={this.props.your_gender}
            setYourGender={(your_gender: YourGender) => {
              this.props.setYourGender(your_gender);
              this.setState({
                isFilled: true
              });
            }}
          />

          <span
            className={[
              styles.Panel_WarningText__style,
              styles.Panel_WarningText__age_layout
            ].join(" ")}
          >
            Age
          </span>

          <AgeSelector
            your_age={this.props.your_age}
            setYourAge={(your_age: YourAge[]) => {
              this.props.setYourAge(your_age);
              this.setState({
                isFilled: true
              });
            }}
          />

          <Button
            text="Next"
            isFilled={true}
            isActive={this.state.isFilled}
            color={ButtonColor.MAIN}
            textColor={ButtonTextColor.WHITE}
            borderRadius8={true}
            buttonType={ButtonType.SIGNUP_PAGE_NEXT}
            onClick={() =>
              this.onFilled(() => this.props.history.push(SIGNUP_3))
            }
          />

          {!this.state.isFilled ? (
            <span
              className={[
                styles.Panel_ErrorMessage__layout,
                styles.Panel_ErrorMessage__style
              ].join(" ")}
            >
              Please, fill all the fields
            </span>
          ) : null}

          <LogInSuggestion isSignUp={true} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  your_age: state.settingsReducer.your_age,
  your_gender: state.settingsReducer.your_gender,

  prevURL: state.routerReducer.previousURL
});

const mapDispatchToProps = (dispatch: any) => ({
  setYourAge: (your_age: YourAge[]) => dispatch(setYourAge(your_age)),
  setYourGender: (your_gender: YourGender) =>
    dispatch(setYourGender(your_gender)),
  setYoursToDefault: () => dispatch(setYoursToDefault()),

  setPrevURL: (prevURL: string) => dispatch(setPrevURL(prevURL))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp_2);
