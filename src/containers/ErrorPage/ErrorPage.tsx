import React, { Component } from "react";

import styles from "./ErrorPage.scss";

class ErrorPage extends Component {
  public render() {
    return (
      <div className={styles.ErrorPage}>
        <span className={styles.ErrorPage_Text__style}>
          Page doesn`t exist :(
        </span>
      </div>
    );
  }
}

export default ErrorPage;
