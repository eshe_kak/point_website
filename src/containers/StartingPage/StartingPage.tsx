import moment from "moment";
import "normalize.css";
import * as React from "react";
import Button from "../../components/UI/Button/Button";

import {
  LOGIN,
  SIGNUP_1,
  STARTING_PAGE
} from "../../../logic/constants/_routeUrls";

import {
  ButtonColor,
  ButtonTextColor,
  ButtonType
} from "../../../logic/enums/enums";

import styles from "./StartingPage.scss";

import { MyGender } from "../../../store/settings/helpers";

import { connect } from "react-redux";

import { IApplicationState } from "../../../store/index";

import { setPrevURL } from "../../../store/router/actions";
import { setMyGender, setNickname } from "../../../store/settings/actions";

import { ISettingsState } from "../../../store/settings/helpers";

import { RouteComponentProps } from "react-router";

// Props passed from mapStateToProps
interface PropsFromState extends ISettingsState {
  nickname: string;
  my_age: moment.Moment;
  my_gender: MyGender;
}

// Props passed from mapDispatchToProps
interface PropsFromDispatch {
  setMyGender: typeof setMyGender;
  setNickname: typeof setNickname;
  setPrevURL: typeof setPrevURL;
}

// Combine both state + dispatch props - as well as any props we want to pass - in a union type.
type SignUp_1Props = PropsFromState &
  PropsFromDispatch &
  RouteComponentProps<any>;

// Component-specific props.
interface OtherProps {
  children: (props: SignUp_1Props) => React.ReactNode;
}

class StartingPage extends React.Component<SignUp_1Props & OtherProps> {
  constructor(props: any) {
    super(props);
  }

  public componentWillUnmount() {
    this.props.setPrevURL(STARTING_PAGE);
  }

  public render() {
    return (
      <div className={styles.StartingPage}>
        <div
          className={[
            styles.StartingPage_Panel,
            styles.StartingPage_Panel__style
          ].join(" ")}
        >
          <div className={styles.Panel_ImageGridItem}>
            <img
              className={styles.ImageGridItem_Image__size}
              src="/static/img/logo.svg"
            />
          </div>

          <div className={styles.Panel_Actions}>
            <div
              className={[
                styles.Actions_Text,
                styles.Actions_Text__marginBottom
              ].join(" ")}
            >
              <span className={styles.Actions_PointText}>POINT</span>
              <span
                className={[
                  styles.Actions_Grey,
                  styles.Actions__mobile_layout
                ].join(" ")}
              >
                The easiest way to meet friends nearby
              </span>
            </div>
            <Button
              text="Sign Up"
              isFilled={true}
              isActive={true}
              color={ButtonColor.MAIN}
              textColor={ButtonTextColor.WHITE}
              borderRadius8={true}
              buttonType={ButtonType.STARTING_PAGE}
              onClick={() => this.props.history.push(SIGNUP_1)}
            />
            <span
              className={[styles.Actions_Grey, styles.Actions_Or].join(" ")}
            >
              or
            </span>
            <Button
              text="Log In"
              isFilled={false}
              isActive={true}
              color={ButtonColor.MAIN}
              textColor={ButtonTextColor.MAIN}
              borderRadius8={true}
              buttonType={ButtonType.STARTING_PAGE}
              onClick={() => this.props.history.push(LOGIN)}
            />
          </div>

          <div className={styles.Panel_Border}>
            <div className={styles.Border} />
          </div>
        </div>

        <div
          className={[
            styles.StartingPage_AppLink,
            styles.StartingPage_AppLink__display_flex,
            styles.StartingPage_AppLink__fontSize_16
          ].join(" ")}
        >
          <span className={styles.AppLink_Text}>
            Install the app using App Store
          </span>
          <img
            className={styles.AppLink_AppStoreIcon}
            src="/static/img/appStoreIcon.svg"
          />
        </div>
      </div>
    );
  }
}

// export default StartingPage

const mapStateToProps = (state: IApplicationState) => ({
  nickname: state.settingsReducer.nickname,
  my_age: state.settingsReducer.my_age,
  my_gender: state.settingsReducer.my_gender
});

const mapDispatchToProps = (dispatch: any) => ({
  setNickname: (nickname: string) => dispatch(setNickname(nickname)),
  setMyGender: (my_age: MyGender) => dispatch(setMyGender(my_age)),
  setPrevURL: (prevURL: string) => dispatch(setPrevURL(prevURL))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartingPage);
