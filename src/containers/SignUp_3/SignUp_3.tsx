import "normalize.css";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";

import Button from "../../components/UI/Button/Button";
import Input from "../../components/UI/Input/Input";
import LogInSuggestion from "../../components/UI/LogInSuggestion/LogInSuggestion";

import { SIGNUP_2, STARTING_PAGE } from "../../../logic/constants/_routeUrls";

import {
  ButtonColor,
  ButtonTextColor,
  ButtonType,
  InputType
} from "../../../logic/enums/enums";

import { _Authorization, _Registration } from "../../../logic/axios/_urls";

import {
  checkPhone,
  createAccount,
  submitSMS
} from "../../../logic/axios/requests";

import styles from "./SignUp_3.scss";

interface Props {
  isSignUp: boolean;
}

interface State {
  phone: string;
  sms: string;
  isTelephonePage: boolean;
  isFilled: boolean;
}

type SignUp_3Props = Props & RouteComponentProps<any>;

class SignUp_3 extends React.Component<SignUp_3Props, State> {
  private privacyPolicy = (
    <label
      className={[
        styles.Panel_Label,
        styles.Panel_Label__layout,
        styles.Panel_Label__size,
        styles.Panel_Label__style
      ].join(" ")}
    >
      <input
        className={styles.Label_Checkbox}
        type="checkbox"
        name="checkbox"
      />
      <span className={styles.Label_Checkbox__custom} />
      <span
        className={[styles.Label_Text__style, styles.Label_Text__layout].join(
          " "
        )}
      >
        I have read and agree to the{" "}
        <span className={styles.LabelText_PrivatePolict__style}>
          Privacy Policy
        </span>
      </span>
    </label>
  );

  private progressPoints = (
    <React.Fragment>
      <img
        className={[
          styles.Panel_ProgressPoint__size,
          styles.Panel_ProgressPointFirst__layout
        ].join(" ")}
        src="/static/img/circleActive.svg"
      />
      <img
        className={[
          styles.Panel_ProgressPoint__size,
          styles.Panel_ProgressPointSecond__layout
        ].join(" ")}
        src="/static/img/circleActive.svg"
      />
      <img
        className={[
          styles.Panel_ProgressPoint__size,
          styles.Panel_ProgressPointThird__layout
        ].join(" ")}
        src="/static/img/circleActive.svg"
      />
    </React.Fragment>
  );

  constructor(props: SignUp_3Props) {
    super(props);
    this.state = {
      phone: "",
      sms: "",
      isTelephonePage: true,
      isFilled: true
    };
  }

  private async checkPhone(pathname: string) {
    const response = await checkPhone(pathname, this.state.phone);

    const status = response.data.payload.status;
    if (status) {
      this.setState({
        isTelephonePage: !status
      });
    }
  }

  private getSMS(pathname: string) {
    const regExp = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/;

    // do some work with promises => change the interface
    const isFilled = regExp.test(this.state.phone);

    this.setState({
      isFilled
    });

    if (isFilled) {
      this.checkPhone(pathname);
    }
  }

  private async createAccount(pathname: string, phone: string, sms: string) {
    const response = await createAccount(pathname, phone, sms);
  }

  private async submitSMS(pathname: string) {
    const response = await submitSMS(
      pathname,
      this.state.phone,
      this.state.sms
    );

    const status = response.data.payload.status;
    if (status) {
      alert("Now you should create new user");
    } else {
      this.setState({
        isFilled: false
      });
    }
  }

  private changeInputState(value: string) {
    this.state.isTelephonePage
      ? this.setState({
          phone: value
        })
      : this.setState({
          sms: value
        });
  }

  public componentWillReceiveProps() {
    this.setState({
      phone: "",
      sms: "",
      isTelephonePage: true,
      isFilled: true
    });
  }

  public render() {
    return (
      <div className={styles.SignUp}>
        <div
          className={[styles.SignUp_Panel, styles.SignUp_Panel__style].join(
            " "
          )}
        >
          <img
            className={[
              styles.Panel_GoBackIcon__size,
              styles.Panel_GoBackIcon__layout,
              styles.Panel_GoBackIcon__style
            ].join(" ")}
            src="/static/img/goBackIcon.svg"
            onClick={
              this.state.isTelephonePage
                ? () =>
                    this.props.history.push(
                      this.props.isSignUp ? SIGNUP_2 : STARTING_PAGE
                    )
                : () =>
                    this.setState({
                      isTelephonePage: true,
                      isFilled: true
                    })
            }
          />

          {this.props.isSignUp ? this.progressPoints : null}

          <span
            className={[
              styles.Panel_HeadText__style,
              styles.Panel_HeadText__layout
            ].join(" ")}
          >
            {this.props.isSignUp ? "Sign Up" : "Log In"}
          </span>

          <span
            className={[
              styles.Panel_InfoText__style,
              styles.Panel_InfoText__layout
            ].join(" ")}
          >
            {this.props.isSignUp
              ? "Verification"
              : "Sign in with your phone number"}
          </span>

          <Input
            value={
              this.state.isTelephonePage ? this.state.phone : this.state.sms
            }
            placeholder={
              this.state.isTelephonePage ? "Phone number" : "Verification code"
            }
            inputType={InputType.SIGNUP_3}
            onChange={(value: string) => {
              this.changeInputState(value);
              this.setState({
                isFilled: true
              });
            }}
          />

          {!this.state.isTelephonePage ? this.privacyPolicy : null}

          <Button
            text={
              this.state.isTelephonePage ? "Get a verification code" : "Go!"
            }
            isFilled={true}
            isActive={this.state.isFilled}
            color={ButtonColor.MAIN}
            textColor={ButtonTextColor.WHITE}
            borderRadius8={true}
            buttonType={ButtonType.SIGNUP_PAGE_3}
            onClick={
              this.state.isTelephonePage
                ? () =>
                    this.getSMS(
                      this.props.isSignUp
                        ? _Registration.CHECK_PHONE
                        : _Authorization.CHECK_PHONE
                    )
                : () =>
                    this.submitSMS(
                      this.props.isSignUp
                        ? _Registration.SUBMIT_SMS
                        : _Authorization.SUBMIT_SMS
                    )
            }
          />

          {!this.state.isFilled ? (
            <span
              className={[
                styles.Panel_ErrorMessage__layout,
                styles.Panel_ErrorMessage__style
              ].join(" ")}
            >
              {this.state.isTelephonePage
                ? "Incorrect phone number"
                : "Incorrect code"}
            </span>
          ) : null}

          <LogInSuggestion isSignUp={this.props.isSignUp} />
        </div>
      </div>
    );
  }
}

export default withRouter(SignUp_3);
