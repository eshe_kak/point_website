import moment from "moment";
import "normalize.css";
import * as React from "react";
import { connect } from "react-redux";
import { Redirect, RouteComponentProps } from "react-router";

import { IApplicationState } from "../../../store/index";
import {
  setMyAge,
  setMyGender,
  setNickname
} from "../../../store/settings/actions";
import { ISettingsState } from "../../../store/settings/helpers";

import { setPrevURL } from "../../../store/router/actions";
import { IRoutersState } from "../../../store/router/helpers";

import Button from "../../components/UI/Button/Button";
import DatePickerCustomized from "../../components/UI/DatePickerCustomized/DatePickerCustomized";
import Input from "../../components/UI/Input/Input";
import LogInSuggestion from "../../components/UI/LogInSuggestion/LogInSuggestion";

import {
  SIGNUP_1,
  SIGNUP_2,
  STARTING_PAGE
} from "../../../logic/constants/_routeUrls";

import {
  ButtonColor,
  ButtonTextColor,
  ButtonType,
  ButtonTypeGender,
  InputType
} from "../../../logic/enums/enums";

import { MyGender } from "../../../store/settings/helpers";

import styles from "./SignUp_1.scss";

// Props passed from mapStateToProps
type PropsFromReducers = ISettingsState & IRoutersState;

interface PropsFromState extends PropsFromReducers {
  nickname: string;
  my_age: moment.Moment;
  my_gender: MyGender;

  prevURL: string;
}

// Props passed from mapDispatchToProps
interface PropsFromDispatch {
  setNickname: typeof setNickname;
  setMyAge: typeof setMyAge;
  setMyGender: typeof setMyGender;

  setPrevURL: typeof setPrevURL;
}

// Combine both state + dispatch props - as well as any props we want to pass - in a union type.
type SignUp_1Props = PropsFromState &
  PropsFromDispatch &
  RouteComponentProps<any>;

// Component-specific props.
interface OtherProps {
  children: (props: SignUp_1Props) => React.ReactNode;
}

interface State {
  isFilled: boolean;
}

class SignUp_1 extends React.Component<SignUp_1Props & OtherProps, State> {
  constructor(props: SignUp_1Props & OtherProps) {
    super(props);

    this.state = {
      isFilled: true
    };
  }

  private onFilled(nextPageSwitch?: Function) {
    const isFilled =
      !!this.props.nickname &&
      this.props.my_age.isBefore(moment().subtract(18, "years")) &&
      (this.props.my_gender === MyGender.MALE ||
        this.props.my_gender === MyGender.FEMALE);

    this.setState(
      {
        isFilled
      },
      () => (this.state.isFilled ? nextPageSwitch() : null)
    );
  }

  public componentWillUnmount() {
    this.props.setPrevURL(SIGNUP_1);
  }

  public render() {
    return (
      <React.Fragment>
        {this.props.prevURL === STARTING_PAGE ||
        this.props.prevURL === SIGNUP_2 ? null : (
          <Redirect to={SIGNUP_1} />
        )}

        <div className={styles.SignUp}>
          <div
            className={[styles.SignUp_Panel, styles.SignUp_Panel__style].join(
              " "
            )}
          >
            <img
              className={[
                styles.Panel_GoBackIcon__size,
                styles.Panel_GoBackIcon__layout,
                styles.Panel_GoBackIcon__style
              ].join(" ")}
              src="/static/img/goBackIcon.svg"
              onClick={() => this.props.history.push(STARTING_PAGE)}
            />

            <span
              className={[
                styles.Panel_HeadText__style,
                styles.Panel_HeadText__layout
              ].join(" ")}
            >
              Sign Up
            </span>

            <img
              className={[
                styles.Panel_ProgressPoint__size,
                styles.Panel_ProgressPointFirst__layout
              ].join(" ")}
              src="/static/img/circleActive.svg"
            />
            <img
              className={[
                styles.Panel_ProgressPoint__size,
                styles.Panel_ProgressPointSecond__layout
              ].join(" ")}
              src="/static/img/circleInactive.svg"
            />
            <img
              className={[
                styles.Panel_ProgressPoint__size,
                styles.Panel_ProgressPointThird__layout
              ].join(" ")}
              src="/static/img/circleInactive.svg"
            />

            <span
              className={[
                styles.Panel_InfoText__style,
                styles.Panel_InfoText__layout
              ].join(" ")}
            >
              Fill in your personal information
            </span>

            <Input
              value={this.props.nickname}
              placeholder="Name"
              inputType={InputType.SIGNUP_1}
              onChange={(value: string) => {
                this.props.setNickname(value);
                this.setState({
                  isFilled: true
                });
              }}
            />

            <span
              className={[
                styles.Panel_WarningText__style,
                styles.Panel_WarningText__gender_layout
              ].join(" ")}
            >
              Gender
            </span>

            <div
              className={[
                styles.Panel_Buttons,
                styles.Panel_Buttons__layout
              ].join(" ")}
            >
              <Button
                text="Male"
                isFilled={false}
                color={ButtonColor.GREY}
                textColor={ButtonTextColor.GREY}
                borderRadius8={false}
                buttonTypeGender={ButtonTypeGender.MALE}
                buttonType={ButtonType.SIGNUP_PAGE_1_GENDER}
                isSelected={this.props.my_gender === MyGender.MALE}
                onClick={() => {
                  this.props.setMyGender(MyGender.MALE);
                  this.setState({
                    isFilled: true
                  });
                }}
              />

              <Button
                text="Female"
                isFilled={false}
                color={ButtonColor.GREY}
                textColor={ButtonTextColor.GREY}
                borderRadius8={false}
                buttonTypeGender={ButtonTypeGender.FEMALE}
                buttonType={ButtonType.SIGNUP_PAGE_1_GENDER}
                isSelected={this.props.my_gender === MyGender.FEMALE}
                onClick={() => {
                  this.props.setMyGender(MyGender.FEMALE);
                  this.setState({
                    isFilled: true
                  });
                }}
              />
            </div>

            <span
              className={[
                styles.Panel_WarningText__style,
                styles.Panel_WarningText__dateOfBirth_layout
              ].join(" ")}
            >
              Date of birth
            </span>

            <DatePickerCustomized
              selectedDate={this.props.my_age}
              onChange={(my_age: moment.Moment) => {
                this.props.setMyAge(my_age);
                this.setState({
                  isFilled: true
                });
              }}
            />

            <Button
              text="Next"
              isFilled={true}
              isActive={this.state.isFilled}
              color={ButtonColor.MAIN}
              textColor={ButtonTextColor.WHITE}
              borderRadius8={true}
              buttonType={ButtonType.SIGNUP_PAGE_NEXT}
              onClick={() =>
                this.onFilled(() => this.props.history.push(SIGNUP_2))
              }
            />

            {/* Error message under the button */}
            {!this.state.isFilled ? (
              <span
                className={[
                  styles.Panel_ErrorMessage__layout,
                  styles.Panel_ErrorMessage__style
                ].join(" ")}
              >
                Please, fill all the fields
              </span>
            ) : null}

            <LogInSuggestion isSignUp={true} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  nickname: state.settingsReducer.nickname,
  my_age: state.settingsReducer.my_age,
  my_gender: state.settingsReducer.my_gender,

  prevURL: state.routerReducer.previousURL
});

const mapDispatchToProps = (dispatch: any) => ({
  setNickname: (nickname: string) => dispatch(setNickname(nickname)),
  setMyAge: (my_age: moment.Moment) => dispatch(setMyAge(my_age)),
  setMyGender: (my_gender: MyGender) => dispatch(setMyGender(my_gender)),

  setPrevURL: (prevURL: string) => dispatch(setPrevURL(prevURL))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp_1);
