export const Avatar__size_border: string;
export const Avatar__size: string;
export const Avatar__style: string;
export const Avatar__style_border: string;
export const Avatar__size_border_male: string;
export const Avatar__size_border_female: string;
export const Avatar__layout: string;
