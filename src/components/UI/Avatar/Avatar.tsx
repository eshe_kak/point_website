import "normalize.css";

import React from "react";

import styles from "./Avatar.scss";

interface Props {
  isMale: boolean;
  srcPathname: string;
}

const Avatar = (props: Props) => {
  const setClassName = () => {
    const className = [
      styles.Avatar__size_border,
      styles.Avatar__style_border,
      styles.Avatar__layout
    ];

    props.isMale
      ? className.push(styles.Avatar__size_border_male)
      : className.push(styles.Avatar__size_border_female);

    return className.join(" ");
  };

  return (
    <div className={setClassName()}>
      <img
        className={[styles.Avatar__size, styles.Avatar__style].join(" ")}
        src={props.srcPathname}
      />
    </div>
  );
};

export default Avatar;
