import "normalize.css";
import React, { Component } from "react";

import styles from "./PointButton.scss";

interface State {
  isActive: boolean;
}

interface Props {
  onActive: Function;
}

class PointButton extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isActive: false
    };
  }

  public render() {
    // Waiting Button
    const waitingButton = (
      <div
        className={[
          styles.PointButton_WaitingPointOuter,
          styles.PointButton_WaitingPointOuter__layout,
          styles.PointButton_WaitingPointOuter__layout_inner,
          styles.PointButton_WaitingPointOuter__size,
          styles.PointButton_WaitingPointOuter__style
        ].join(" ")}
        onClick={() => {
          this.setState({
            isActive: true
          });
          this.props.onActive();
        }}
      >
        {/* Middle Waiting Button  */}
        <div
          className={[
            styles.PointButton_WaitingPointMiddle__layout_inner,
            styles.PointButton_WaitingPointMiddle__size,
            styles.PointButton_WaitingPointMiddle__style
          ].join(" ")}
        >
          {/* Inner Waiting Button */}
          <div
            className={[
              styles.PointButton_WaitingPointInner__size,
              styles.PointButton_WaitingPointInner__style
            ].join(" ")}
          />
        </div>
      </div>
    );

    // Active Button
    const activeButton = (
      <React.Fragment>
        <div
          className={[
            styles.PointButton_ActivePoint__layout,
            styles.PointButton_ActivePoint__size,
            styles.PointButton_ActivePoint__style
          ].join(" ")}
        />
        <div
          className={[
            styles.PointButton_ActivePoint__layout,
            styles.PointButton_ActivePoint__size,
            styles.PointButton_ActivePoint__style,
            styles.PointButton_ActivePoint__style_animation
          ].join(" ")}
        />
        <div
          className={[
            styles.PointButton_ActivePoint__layout,
            styles.PointButton_ActivePoint__size,
            styles.PointButton_ActivePoint__style,
            styles.PointButton_ActivePoint__style_animation,
            styles.PointButton_ActivePoint__style_animationDelay500
          ].join(" ")}
        />
        <div
          className={[
            styles.PointButton_ActivePoint__layout,
            styles.PointButton_ActivePoint__size,
            styles.PointButton_ActivePoint__style,
            styles.PointButton_ActivePoint__style_animation,
            styles.PointButton_ActivePoint__style_animationDelay1000
          ].join(" ")}
        />
        <div
          className={[
            styles.PointButton_ActivePoint__layout,
            styles.PointButton_ActivePoint__size,
            styles.PointButton_ActivePoint__style,
            styles.PointButton_ActivePoint__style_animation,
            styles.PointButton_ActivePoint__style_animationDelay1500
          ].join(" ")}
        />
      </React.Fragment>
    );

    return (
      <div
        className={[styles.PointButton__layout, styles.PointButton__size].join(
          " "
        )}
      >
        {this.state.isActive ? activeButton : waitingButton}
      </div>
    );
  }
}

export default PointButton;
