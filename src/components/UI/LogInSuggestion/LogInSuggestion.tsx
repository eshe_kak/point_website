import React from "react";
import { Link } from "react-router-dom";

import { LOGIN, SIGNUP_1 } from "../../../../logic/constants/_routeUrls";

import styles from "./LogInSuggestion.scss";

interface Props {
  isSignUp: boolean;
}

const LogInSuggestion = (props: Props) => {
  return (
    <div
      className={[
        styles.Panel_BottomInfoPanel__size,
        styles.Panel_BottomInfoPanel__layout,
        styles.Panel_BottomInfoPanel__style
      ].join(" ")}
    >
      <span className={styles.BottomInfoPanel_InfoText__style}>
        {props.isSignUp
          ? "Already have an account? "
          : "Do not have an account? "}
        <Link
          className={styles.InfoText_Link__style}
          to={props.isSignUp ? LOGIN : SIGNUP_1}
        >
          {props.isSignUp ? "Log In" : "Sign Up"}
        </Link>
      </span>
    </div>
  );
};

export default LogInSuggestion;
