import React from "react";

import moment from "moment";
import DatePicker from "react-datepicker";

import styles from "./AdditionalDPC.scss";
import "./DatePickerCustomized.scss";

interface Props {
  selectedDate: moment.Moment;
  onChange: Function;
}
interface State {
  // startDate: moment.Moment,
  // selectedDate: moment.Moment,
  // minDate:  moment.Moment
}

class DatePickerCustomized extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      // startDate: moment(),
      // selectedDate: moment(),
      // minDate: moment().subtract(100, 'years')
    };
  }

  public render() {
    return (
      <div
        className={[
          styles.DatePickerCustomized__layout_signUp,
          styles.DatePickerCustomized__size_signUp
        ].join(" ")}
      >
        <DatePicker
          selected={this.props.selectedDate}
          onChange={() => console.log()}
          dateFormat="DD.MM.YYYY"
          maxDate={moment()}
          minDate={moment().subtract(100, "years")}
          showYearDropdown={true}
          onSelect={(date: moment.Moment) => this.props.onChange(date)}
        />

        <img
          className={[
            styles.DatePickerCustomized_EditIcon__size,
            styles.DatePickerCustomized_EditIcon__layout,
            styles.DatePickerCustomized_EditIcon__style
          ].join(" ")}
          src="/static/img/pencil-alt-solid.svg"
        />
      </div>
    );
  }
}

export default DatePickerCustomized;
