import "normalize.css";
import React from "react";

import Avatar from "../Avatar/Avatar";

import styles from "./SideBarMenu.scss";

const SideBarMenu = () => {
  return (
    <div
      className={[
        styles.SideBarMenu__size,
        styles.SideBarMenu__layout,
        styles.SideBarMenu__style,
        styles.SideBarMenu__layout_inner,
        styles.SideBarMenu__style_text
      ].join(" ")}
    >
      <Avatar isMale={false} srcPathname={"/static/img/arisha.jpg"} />

      <span className={styles.SideBar_Name__layout}>Arisha</span>

      <span
        className={[
          styles.SideBar_Description__style,
          styles.SideBar_Description__layout
        ].join(" ")}
      >
        Write a few words about yourself to raise match chance
      </span>

      <div
        className={[
          styles.SideBarMenu_MenuItems__layout,
          styles.SideBarMenu_MenuItems__size
        ].join(" ")}
      >
        <div
          className={[
            styles.MenuItems_Item,
            styles.MenuItems_Item__size,
            styles.MenuItems_Item__layout_inner,
            styles.MenuItems_Item__style_borderLeft
          ].join(" ")}
        >
          <img
            className={[
              styles.Item_Icon__size,
              styles.Item_Icon__layout,
              styles.Item_Icon__layout_active
            ].join(" ")}
            src="/static/img/home.svg"
          />
          <span
            className={[styles.Item_Text_style, styles.Item_Text__layout].join(
              " "
            )}
          >
            Home
          </span>
        </div>
        <div
          className={[
            styles.MenuItems_Item,
            styles.MenuItems_Item__size,
            styles.MenuItems_Item__layout_messages,
            styles.MenuItems_Item__layout_inner
          ].join(" ")}
        >
          <img
            className={[styles.Item_Icon__size, styles.Item_Icon__layout].join(
              " "
            )}
            src="/static/img/messages.svg"
          />
          <span
            className={[styles.Item_Text_style, styles.Item_Text__layout].join(
              " "
            )}
          >
            Messages
          </span>
        </div>
        <div
          className={[
            styles.MenuItems_Item,
            styles.MenuItems_Item__size,
            styles.MenuItems_Item__layout_settings,
            styles.MenuItems_Item__layout_inner
          ].join(" ")}
        >
          <img
            className={[styles.Item_Icon__size, styles.Item_Icon__layout].join(
              " "
            )}
            src="/static/img/settings.svg"
          />
          <span
            className={[styles.Item_Text_style, styles.Item_Text__layout].join(
              " "
            )}
          >
            Settings
          </span>
        </div>
      </div>

      <div
        className={[
          styles.SideBar_BottomImage__size,
          styles.SideBar_BottomImage__style,
          styles.SideBar_BottomImage__layout
        ].join(" ")}
      />
    </div>
  );
};

export default SideBarMenu;
