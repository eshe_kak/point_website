import "normalize.css";
import * as React from "react";
import Button from "../../Button/Button";

import {
  ButtonColor,
  ButtonTextColor,
  ButtonType
} from "../../../../../logic/enums/enums";

import { YourAge } from "../../../../../store/settings/helpers";

import styles from "./AgeSelector.scss";

interface Props {
  your_age: YourAge[];
  setYourAge: Function;
}

class AgeSelector extends React.Component<Props, any> {
  private onSelected(value: YourAge): number {
    return this.props.your_age.indexOf(value);
  }

  private select(value: YourAge) {
    const index = this.onSelected(value);
    let newArray = [...this.props.your_age];

    if (index > -1) {
      newArray.splice(index, 1);
      const allIndex = this.onSelected(YourAge.ALL);
      console.log("allIndex " + allIndex);
      if (allIndex > -1) {
        newArray.splice(allIndex - 1, 1);
        console.log(newArray);
      }
    } else {
      if (value === YourAge.ALL) {
        newArray = [
          YourAge.YOUNG,
          YourAge.STILL_YOUNG,
          YourAge.MIDDLE,
          YourAge.PREMATURE,
          YourAge.MATURE,
          YourAge.ALL
        ];
      } else {
        newArray.push(value);
      }
    }

    this.props.setYourAge(newArray);
  }
  public render() {
    return (
      <div
        className={[
          styles.AgeSelector__size_signUp2,
          styles.AgeSelector__layout_signUp2
        ].join(" ")}
      >
        <Button
          text="18 - 22"
          isFilled={false}
          isActive={true}
          color={ButtonColor.GREY}
          textColor={ButtonTextColor.GREY}
          borderRadius8={false}
          buttonType={ButtonType.SIGNUP_PAGE_2_AGE_1}
          isSelected={this.onSelected(YourAge.YOUNG) > -1}
          onClick={() => this.select(YourAge.YOUNG)}
        />

        <Button
          text="23 - 27"
          isFilled={false}
          isActive={true}
          color={ButtonColor.GREY}
          textColor={ButtonTextColor.GREY}
          borderRadius8={false}
          buttonType={ButtonType.SIGNUP_PAGE_2_AGE_2}
          isSelected={this.onSelected(YourAge.STILL_YOUNG) > -1}
          onClick={() => this.select(YourAge.STILL_YOUNG)}
        />

        <Button
          text="28 - 35"
          isFilled={false}
          isActive={true}
          color={ButtonColor.GREY}
          textColor={ButtonTextColor.GREY}
          borderRadius8={false}
          buttonType={ButtonType.SIGNUP_PAGE_2_AGE_3}
          isSelected={this.onSelected(YourAge.MIDDLE) > -1}
          onClick={() => this.select(YourAge.MIDDLE)}
        />

        <Button
          text="36 - 45"
          isFilled={false}
          isActive={true}
          color={ButtonColor.GREY}
          textColor={ButtonTextColor.GREY}
          borderRadius8={false}
          buttonType={ButtonType.SIGNUP_PAGE_2_AGE_4}
          isSelected={this.onSelected(YourAge.PREMATURE) > -1}
          onClick={() => this.select(YourAge.PREMATURE)}
        />

        <Button
          text="46 +"
          isFilled={false}
          isActive={true}
          color={ButtonColor.GREY}
          textColor={ButtonTextColor.GREY}
          borderRadius8={false}
          buttonType={ButtonType.SIGNUP_PAGE_2_AGE_5}
          isSelected={this.onSelected(YourAge.MATURE) > -1}
          onClick={() => this.select(YourAge.MATURE)}
        />

        <Button
          text="All ages"
          isFilled={false}
          isActive={true}
          color={ButtonColor.GREY}
          textColor={ButtonTextColor.GREY}
          borderRadius8={false}
          buttonType={ButtonType.SIGNUP_PAGE_2_AGE_6}
          isSelected={this.onSelected(YourAge.ALL) > -1}
          onClick={() => this.select(YourAge.ALL)}
        />
      </div>
    );
  }
}

export default AgeSelector;
