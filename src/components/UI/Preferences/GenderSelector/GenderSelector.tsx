import "normalize.css";
import * as React from "react";
import { YourGender } from "../../../../../store/settings/helpers";

interface Props {
  selectedGender: YourGender;
  setYourGender: Function;
}

import * as styles from "./GenderSelector.scss";

class GenderSelector extends React.Component<Props, any> {
  private lineRef1: React.RefObject<HTMLDivElement>;
  private lineRef2: React.RefObject<HTMLDivElement>;
  private lineRef3: React.RefObject<HTMLDivElement>;

  private checkIconSrc = "/static/img/checkIcon.svg";
  private checkIconActiveSrc = "/static/img/checkIconActive.svg";

  constructor(props: Props) {
    super(props);
    this.lineRef1 = React.createRef();
    this.lineRef2 = React.createRef();
    this.lineRef3 = React.createRef();
  }

  private onMouseOver(currentRef: React.RefObject<HTMLDivElement>) {
    currentRef.current.querySelector("img").src = this.checkIconActiveSrc;
  }

  private onMouseOut(
    currentRef: React.RefObject<HTMLDivElement>,
    isSelected: boolean
  ) {
    currentRef.current.querySelector("img").src = isSelected
      ? this.checkIconActiveSrc
      : this.checkIconSrc;
  }

  private generateItem(
    name: string,
    withBottomLine: boolean,
    refNumber: number,
    your_gender?: YourGender
  ) {
    const lineClassNames = [
      styles.GenderSelector_Line,
      styles.GenderSelector_Line__size
    ];

    const isSelected =
      this.props.selectedGender === YourGender.ALL ||
      this.props.selectedGender === your_gender;

    let currentRef: React.RefObject<HTMLDivElement>;

    // Maybe it is not the best solution
    switch (refNumber) {
      case 1:
        currentRef = this.lineRef1;
        break;
      case 2:
        currentRef = this.lineRef2;
        break;
      case 3:
        currentRef = this.lineRef3;
        break;
      default:
        break;
    }

    if (isSelected) {
      lineClassNames.push(styles.GenderSelector_Line__style_blackTextColor);

      if (withBottomLine) {
        lineClassNames.push(
          styles.GenderSelector_Line__style_bottomBorderBlack
        );
      }
    } else {
      lineClassNames.push(styles.GenderSelector_Line__style_textColor);

      if (withBottomLine) {
        lineClassNames.push(styles.GenderSelector_Line__style_bottomBorder);
      }
    }

    const imgSrc = isSelected ? this.checkIconActiveSrc : this.checkIconSrc;

    return (
      <div
        className={lineClassNames.join(" ")}
        ref={currentRef}
        onMouseOver={() => this.onMouseOver(currentRef)}
        onMouseOut={() => this.onMouseOut(currentRef, isSelected)}
        onClick={() => this.props.setYourGender(your_gender)}
      >
        <span className={styles.Line_Text__style}>{name}</span>
        <img
          className={[
            styles.Line_CheckIcon__style,
            styles.Line_CheckIcon__layout
          ].join(" ")}
          src={imgSrc}
        />
      </div>
    );
  }

  public render() {
    return (
      <div
        className={[
          styles.GenderSelector__size,
          styles.GenderSelector__layout_signUp2
        ].join(" ")}
      >
        {this.generateItem("Male", true, 1, YourGender.MALE)}
        {this.generateItem("Female", true, 2, YourGender.FEMALE)}
        {this.generateItem("All", false, 3, YourGender.ALL)}
      </div>
    );
  }
}

export default GenderSelector;
