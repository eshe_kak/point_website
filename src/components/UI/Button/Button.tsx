import * as React from "react";

import styles from "./Button.scss";

import {
  ButtonColor,
  ButtonTextColor,
  ButtonType,
  ButtonTypeGender
} from "../../../../logic/enums/enums";

interface Props {
  text: string;
  isFilled: boolean;
  isActive?: boolean;
  color: ButtonColor;
  textColor: ButtonTextColor;
  borderRadius8: boolean;
  buttonTypeGender?: ButtonTypeGender;
  buttonType: ButtonType;
  isSelected?: boolean;
  onClick?: Function;
}

class Button extends React.Component<Props, any> {
  // constructor(props: Props){
  //     super(props);
  // }

  private createButtonClassNames(): string[] {
    let classNames: string[] = [styles.Button];

    if (this.props.borderRadius8) {
      classNames.push(styles.Button__borderRadius_8);
    } else {
      classNames.push(styles.Button__borderRadius_5);
    }

    if (this.props.isFilled) {
      this.props.isActive
        ? classNames.push(styles.Button__background_mainColor)
        : classNames.push(styles.Button__background_greyColor);
    } else {
      switch (this.props.color) {
        case ButtonColor.MAIN:
          classNames.push(styles.Button__border_mainColor);
          break;
        case ButtonColor.GREY:
          switch (this.props.buttonTypeGender) {
            case ButtonTypeGender.MALE:
              this.props.isSelected
                ? classNames.push(styles.Button__border_male_selected)
                : classNames.push(styles.Button__border_male);
              break;
            case ButtonTypeGender.FEMALE:
              this.props.isSelected
                ? classNames.push(styles.Button__border_female_selected)
                : classNames.push(styles.Button__border_female);
              break;
            default:
              classNames.push(styles.Button__border_greyTextColor);
              break;
          }

          this.props.isSelected
            ? classNames.push(styles.Button__border_greyTextColorSelected)
            : classNames.push(styles.Button__border_greyTextColor);

          break;
        default:
          break;
      }
    }

    switch (this.props.buttonType) {
      case ButtonType.STARTING_PAGE:
        classNames.push(styles.Button__size_startingPage);
        return classNames;
      case ButtonType.SIGNUP_PAGE_1_GENDER:
        classNames.push(styles.Button__size_signUpPageGender);
        return classNames;
      case ButtonType.SIGNUP_PAGE_NEXT:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPageNext,
          styles.Button__layout_signUpPageNext
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_2_AGE_1:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPage2,
          styles.Button__layout_signUpPage2Age1
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_2_AGE_2:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPage2,
          styles.Button__layout_signUpPage2Age2
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_2_AGE_3:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPage2,
          styles.Button__layout_signUpPage2Age3
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_2_AGE_4:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPage2,
          styles.Button__layout_signUpPage2Age4
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_2_AGE_5:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPage2,
          styles.Button__layout_signUpPage2Age5
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_2_AGE_6:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPage2,
          styles.Button__layout_signUpPage2Age6
        ];
        return classNames;
      case ButtonType.SIGNUP_PAGE_3:
        classNames = [
          ...classNames,
          styles.Button__size_signUpPageNext,
          styles.Button__layout_signUpPage3
        ];
        return classNames;
      default:
        return classNames;
    }
  }

  private createButtonTextClassNames(): string[] {
    const classNames: string[] = [styles.Button_Text];

    switch (this.props.textColor) {
      case ButtonTextColor.WHITE:
        classNames.push(styles.Button_Text__color_white);
        break;
      case ButtonTextColor.MAIN:
        classNames.push(styles.Button_Text__color_mainColor);
        break;
      case ButtonTextColor.GREY:
        this.props.isSelected
          ? classNames.push(styles.Button_Text__color_greyTextColorSelected)
          : classNames.push(styles.Button_Text__color_greyTextColor);
        break;
      default:
        break;
    }

    const arrayOfSignUpPage2Classes: ButtonType[] = [
      ButtonType.SIGNUP_PAGE_2_AGE_1,
      ButtonType.SIGNUP_PAGE_2_AGE_2,
      ButtonType.SIGNUP_PAGE_2_AGE_3,
      ButtonType.SIGNUP_PAGE_2_AGE_4,
      ButtonType.SIGNUP_PAGE_2_AGE_5,
      ButtonType.SIGNUP_PAGE_2_AGE_6
    ];

    if (arrayOfSignUpPage2Classes.indexOf(this.props.buttonType) > -1) {
      classNames.push(styles.Button_Text__size_signUpPage2);
    }

    switch (this.props.buttonType) {
      case ButtonType.STARTING_PAGE:
        classNames.push(styles.Button_Text__size_startingPage);
        return classNames;
      case ButtonType.SIGNUP_PAGE_3:
        classNames.push(styles.Button_Text__size_signUpPage3);
        return classNames;
      case ButtonType.SIGNUP_PAGE_1_GENDER:
        this.props.isSelected
          ? classNames.push(styles.Button_Text__size_signUpPageGenderSelected)
          : classNames.push(styles.Button_Text__size_signUpPageGender);
        return classNames;
      default:
        return classNames;
    }
  }

  private createButton(): React.ReactElement<any> {
    const buttonText: React.ReactElement<any> = (
      <span className={this.createButtonTextClassNames().join(" ")}>
        {this.props.text}
      </span>
    );

    return (
      <div
        className={this.createButtonClassNames().join(" ")}
        onClick={() => this.props.onClick()}
      >
        {buttonText}
      </div>
    );
  }
  public render() {
    return this.createButton();
    // <div>Hey</div>
  }
}

export default Button;
