import "normalize.css";
import * as React from "react";

import { InputType } from "../../../../logic/enums/enums";

import styles from "./Input.scss";

interface Props {
  value?: string;
  placeholder: string;
  inputType: InputType;
  onChange: Function;
}

class Input extends React.Component<Props, any> {
  private defineClassName(defaultClassNames: string[]): string[] {
    switch (this.props.inputType) {
      case InputType.SIGNUP_1:
        defaultClassNames.push(styles.Input__layout_signUp1);
        break;
      case InputType.SIGNUP_3:
        defaultClassNames.push(styles.Input__layout_signUp3);
        break;
      default:
        break;
    }

    return defaultClassNames;
  }
  public render() {
    const defaultClassNames: string[] = [
      styles.Input__borders,
      styles.Input__text,
      styles.Input__size
    ];

    return (
      <input
        className={this.defineClassName(defaultClassNames).join(" ")}
        value={this.props.value}
        placeholder={this.props.placeholder}
        onChange={event => this.props.onChange(event.target.value)}
      />
    );
  }
}

export default Input;
