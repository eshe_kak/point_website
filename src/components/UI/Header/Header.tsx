import "normalize.css";
import React from "react";

import styles from "./Header.scss";

const Header = () => {
  return (
    <div
      className={[
        styles.Header__size,
        styles.Header__style,
        styles.Header__layout,
        styles.Header__layout_inner
      ].join(" ")}
    >
      <img
        className={styles.Header_Logo__size}
        src="/static/img/logotype.png"
      />
    </div>
  );
};

export default Header;
