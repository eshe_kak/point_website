import "normalize.css";
import React, { Component } from "react";

import styles from "./Suggestion.scss";

class Suggestion extends Component {
  public render() {
    return (
      <div
        className={[styles.Suggestion__size, styles.Suggestion__style].join(
          " "
        )}
      />
    );
  }
}

export default Suggestion;
