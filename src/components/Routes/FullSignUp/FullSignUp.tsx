import * as React from "react";
import { Route, Switch, withRouter } from "react-router-dom";

import asyncComponent from "../../../../logic/hoc/asyncComponent/asyncComponent";

import {
  SIGNUP_1,
  SIGNUP_2,
  SIGNUP_3
} from "../../../../logic/constants/_routeUrls";

const AsyncSignUp_1: any = asyncComponent(() => {
  return import("../../../containers/SignUp_1/SignUp_1");
});

const AsyncSignUp_2: any = asyncComponent(() => {
  return import("../../../containers/SignUp_2/SignUp_2");
});

const AsyncSignUp_3: any = asyncComponent(() => {
  return import("../../../containers/SignUp_3/SignUp_3");
});

const AsyncErrorPage: any = asyncComponent(() => {
  return import("../../../containers/ErrorPage/ErrorPage");
});

function FullSignUp(): React.ReactElement<any> {
  return (
    <Switch>
      <Route exact={true} path={SIGNUP_1} component={AsyncSignUp_1} />
      <Route exact={true} path={SIGNUP_2} component={AsyncSignUp_2} />
      <Route
        exact={true}
        path={SIGNUP_3}
        render={() => <AsyncSignUp_3 isSignUp={true} />}
      />
      <Route component={AsyncErrorPage} />
    </Switch>
  );
}

export default withRouter(FullSignUp);
