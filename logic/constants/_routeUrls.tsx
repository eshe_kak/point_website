"use-strict";

export const STARTING_PAGE = "/";
export const FULL_SIGNUP = "/signup";
export const SIGNUP_1 = "/signup/1";
export const SIGNUP_2 = "/signup/2";
export const SIGNUP_3 = "/signup/3";
export const LOGIN = "/login";
export const HOME = "/home";

// export {
//     STARTING_PAGE,
//     FULL_SIGNUP,
//     SIGNUP_1,
//     SIGNUP_2,
//     SIGNUP_3
// };
