"use-strict";

enum ButtonColor {
  MAIN,
  GREY
}

enum ButtonTextColor {
  MAIN,
  WHITE,
  GREY
}

enum ButtonTypeGender {
  MALE,
  FEMALE
}

enum ButtonType {
  STARTING_PAGE,
  SIGNUP_PAGE_1_GENDER,
  SIGNUP_PAGE_2_AGE_1,
  SIGNUP_PAGE_2_AGE_2,
  SIGNUP_PAGE_2_AGE_3,
  SIGNUP_PAGE_2_AGE_4,
  SIGNUP_PAGE_2_AGE_5,
  SIGNUP_PAGE_2_AGE_6,
  SIGNUP_PAGE_3,
  SIGNUP_PAGE_NEXT
}

enum InputType {
  SIGNUP_1,
  SIGNUP_3
}

export {
  ButtonColor,
  ButtonTextColor,
  ButtonTypeGender,
  ButtonType,
  InputType
};
