export const MAIN_URL =
  "http://ec2-18-196-137-189.eu-central-1.compute.amazonaws.com";

export enum _Registration {
  CHECK_PHONE = "/registration/checkphone",
  SUBMIT_SMS = "/registration/submitsms",
  CREATE_ACCOUNT = "/registration/createaccount"
}

export enum _Authorization {
  CHECK_PHONE = "/login/checkphone",
  SUBMIT_SMS = "/login/submitsms"
}
