import axios from "axios";

import { MAIN_URL } from "./_urls";

// accepts both pathnames for login and signup
export function checkPhone(pathname: string, phone: string) {
  return axios.post(MAIN_URL + pathname, {
    payload: {
      phone
    }
  });
}

// accepts both pathnames for login and signup
export function submitSMS(pathname: string, phone: string, sms: string) {
  return axios.post(MAIN_URL + pathname, {
    payload: {
      phone,
      sms
    }
  });
}

export function createAccount(pathname: string, phone: string, sms: string) {
  return axios.post(MAIN_URL + pathname, {
    payload: {
      phone,
      sms
    }
  });
}
