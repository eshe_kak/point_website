import React, { Component } from "react";

interface Props {
  isSignUp?: boolean;
}

interface State {
  component: any;
}

const asyncComponent = (importComponent: any) => {
  return class extends Component<Props, State> {
    constructor() {
      super({});

      this.state = {
        component: null
      };
    }

    componentDidMount() {
      importComponent().then((cmp: any) => {
        this.setState({ component: cmp.default });
      });
    }

    render() {
      const C = this.state.component;
      return C ? <C {...this.props} /> : null;
    }
  };
};

export default asyncComponent;
